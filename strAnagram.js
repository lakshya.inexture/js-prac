const str = ["eat", "tea", "tan", "ate", "nat", "bat"];
const multiOccu = [];
const out = [];

// sorted str alphabetically
str.forEach((element) => {
    multiOccu.push(element.split("").sort().join(""));
});

// created set to remove same occurences from multiOccu
const set = new Set(multiOccu);
console.log(set);
const multiOccuSet = [...set];

function findAnagram(arr, val) {
    // create temp array for matching values from multiOccu and multiOccuSet
    const temp = [];
    arr.forEach((e, i) => {
        // if value of both matches, push str value to temp
        if (arr[i] === val) {
            temp.push(str[i]);
        }
    });
    console.log(temp);
    // push temp value to out
    out.push(temp);
}

// pass values of multiOccu and multiOccuSet to findAnagram
multiOccuSet.map((e) => {
    findAnagram(multiOccu, e);
});

console.log(out);
