// function fb() {
//     for (let count = 1; count <= 100; count++) {
//         if (count % 3 === 0 && count % 5 === 0) {
//             console.log("fizzBuzz");
//         } else if (count % 5 === 0) {
//             console.log("Buzz");
//         } else if (count % 3 === 0) {
//             console.log("fizz");
//         } else {
//             console.log(count);
//         }
//     }
// }
// fb();
function fb() {
    for (let count = 1; count <= 100; count++) {
        count % 15 === 0
            ? console.log("fizzBuzz")
            : count % 5 === 0
            ? console.log("Buzz")
            : count % 3 === 0
            ? console.log("fizz")
            : console.log(count);
    }
}
fb();

const sum = ([...arg]) => {
    let even = 0,
        odd = 0;
    arg.forEach((i) => {
        if (i % 2 === 0) {
            even += i;
        } else {
            odd += i;
        }
    });
    console.log([even, odd]);
};

sum([1, 2, 3, 4, 5, 6, 7, 8, 9]);
