// Check if Number is Prime
const checkPrime = (val) => {
    // console.log("val", val);
    let count = 0;
    for (i = 2; i <= val / 2; i++) {
        if (val % i === 0) count += 1;
    }
    if (count === 0) {
        return true;
    } else {
        return false;
    }
};

// Check if number is Cyclic prime
const checkCyclic = (num) => {
    console.log("Number: ", num);
    let permuts = permutator(num);

    if (permuts.length === 0) {
        return false;
    }

    return true;
};

// Get all the cyclic permutations
function permutator(N) {
    var num = N;
    let n = N.toString().length;
    let res = [];
    while (true) {
        // console.log(checkPrime(num));
        if (!checkPrime(num)) return [];
        res.push(num);
        // console.log(num, res);

        var rem = num % 10;
        var dev = parseInt(num / 10);
        num = parseInt(Math.pow(10, n - 1) * rem + dev);
        if (num == N) break;
    }
    return res;
}
let start = new Date();
// Get all the prime numbers in the range
let primes = [];
for (let i = 100; i < 1000000; i++) {
    if (checkPrime(i)) {
        primes.push(i);
    }
}

// Main function
let res = [];
primes.map((prime) => {
    if (checkCyclic(prime)) {
        res.push(prime);
    }
});

let end = new Date();
console.log(res, res.length);
console.log((end.getTime() - start.getTime()) / 1000 + "sec");
